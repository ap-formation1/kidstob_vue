import {defineStore} from "pinia";
import {ref} from "vue";

export const useProfilesStore = defineStore('profiles', () => {
    const profiles = ref({
        new: [],
        babies: [],
        kids: [],
        teenagers: [],
    });
    const errorMessage = ref('');
    const isLoaderActive = ref(true);
    async function getProfilesFromCategory(category) {
        try {
            const response = await fetch(`/data/profiles/${category}/${category}.json`);
            if (!response.ok) {
                throw new Error();
            }
            profiles.value[category] = await response.json();
            isLoaderActive.value = false;
        } catch (error) {
            errorMessage.value = 'Erreur dans la récupération des profils merci de réessayer plus tard';
        }
    }
    function getProfile(category, id){
        return profiles.value[category].find(profile => {
            return profile.id === parseInt(id);
        });
    }

    return {
        profiles,
        errorMessage,
        isLoaderActive,
        getProfilesFromCategory,
        getProfile
    }
});
