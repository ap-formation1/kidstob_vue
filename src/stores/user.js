import {defineStore} from "pinia";
import {computed, ref} from "vue";

export const useUserStore = defineStore('user', () => {

    const user = ref({});

    function setConnectedUser(loggedUser) {
        user.value = {
            lastname: loggedUser.lastname,
            firstname: loggedUser.firstname,
            mail: loggedUser.mail,
            phone: loggedUser.phone,
            address: loggedUser.address,
            city: loggedUser.city,
            zipcode: loggedUser.zipcode,
            image: loggedUser.image
        }
    }

    function deleteUser() {
        user.value = {};
    }
    async function fetchConnectedUser() {
        try {
            const response = await fetch('/data/users/users.json');
            if (!response.ok) {
                throw new Error();
            }
            const users = await response.json();

            const token = localStorage.getItem('token');

            const connectedUser = users.find(user => {
                return user.token === token;
            })

            setConnectedUser(connectedUser);
        } catch (error) {
            return false;
        }
    }

    const computedUser = computed(() => user.value);

    const isConnectedUser = ref(false);

    return {
        user: computedUser,
        setConnectedUser,
        fetchConnectedUser,
        deleteUser,
        isConnectedUser
    }
});
