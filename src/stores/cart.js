import {defineStore} from "pinia";
import {computed, ref} from "vue";

export const useCartStore = defineStore('cart', () => {
    const products = ref([]);
    const shipments = ref([
        {
            option : "relais colis",
            cost : 5
        },
        {
            option : "domicile",
            cost: 12
        },
        {
            option : "livraison express",
            cost : 18
        }
    ]);

    function addNewProduct(id, name, qty, price, desc){
        products.value.push({
            id: id,
            name: name,
            qty: qty,
            price: price,
            totalPrice: qty * price,
            desc: desc
        })
    }

    function changeQuantity(id, event) {
        const product = products.value.find(product => product?.id === parseInt(id))
        product.qty = parseInt(event.target.value);
        updateTotalPrice(product);
    }

    function updateTotalPrice(product){
        product.totalPrice = product.qty * product.price;
    }

    const getTotalCartPrice = computed(() => {
        let total = 0;
        products.value.forEach(product => {
            total += product?.totalPrice;
        })
        return total;
    })

    function deleteProduct(id) {
        const index = products.value.findIndex(product => product?.id === parseInt(id));
        if (index !== -1) {
            products.value.splice(index, 1);
        }
    }
    return {
        products,
        shipments,
        addNewProduct,
        changeQuantity,
        deleteProduct,
        getTotalCartPrice
    };
})