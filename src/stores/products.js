import {defineStore} from "pinia";
import {ref} from "vue";

export const useProductsStore = defineStore('products', () => {
    const products = ref([]);
    const errorMessage = ref('');
    const isLoaderActive = ref(true);
    async function getProducts() {
        try {
            const response = await fetch('/data/products/all/products.json');
            if (!response.ok) {
                throw new Error();
            }
            products.value = await response.json();
            isLoaderActive.value = false;
        } catch (error) {
            errorMessage.value = 'Erreur dans la récupération des produits merci de réessayer plus tard';
        }
    }
    function getProduct(id){
        return products.value.find(product => {
            return product.id === parseInt(id);
        });
    }

    return {
        products,
        errorMessage,
        isLoaderActive,
        getProducts,
        getProduct
    }
});
