import {createRouter, createWebHistory} from 'vue-router'
import Home from '../components/Home.vue'
import Products from "@/components/Products.vue";
import Cart from "@/components/Cart.vue";
import Connexion from "@/components/Connexion.vue";
import Inscription from "@/components/Inscription.vue";
import Contact from "@/components/Contact.vue";
import Payment from "@/components/Payment.vue";
import Product from "@/components/Product.vue";
import Profiles from "@/components/Profiles.vue";
import Profile from "@/components/Profile.vue";
import AccountPage from "@/components/AccountPage.vue";
import Mailbox from "@/components/Mailbox.vue";
import Childcare from "@/components/Childcare.vue";
import LastOrders from "@/components/LastOrders.vue";
import {useUserStore} from "@/stores/user.js";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {title: 'Accueil'}
        },
        {
            path: '/products',
            name: 'products',
            component: Products,
            meta: {title: 'Accessoires'}
        },
        {
            path: '/products/:id',
            name: 'product',
            component: Product,
            meta: {title: 'Détail produit'}
        },
        {
            path: '/profiles/:category',
            name: 'profiles',
            component: Profiles,
        },
        {
            path: '/profile/:category/:id',
            name: 'profile',
            component: Profile,
            meta: {title: 'Profil'}
        },
        {
            path: '/cart',
            name: 'cart',
            component: Cart,
            meta: {title: 'Panier'}
        },
        {
            path: '/connexion',
            name: 'sign-in',
            component: Connexion,
            meta: {title: 'Connexion'}
        },
        {
            path: '/inscription',
            name: 'sign-up',
            component: Inscription,
            meta: {title: 'Inscription'}
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact,
            meta: {title: 'Contact'}
        },
        {
            path: '/payment',
            name: 'payment',
            component: Payment,
            meta: {title: 'Paiement'}
        },
        {
            path: '/account',
            name: 'account',
            component: AccountPage,
            meta: {title: 'Espace Membre'}
        },
        {
            path: '/my-childcares',
            name: 'childcares',
            component: Childcare,
            meta: {title: 'Mes gardes'}
        },
        {
            path: '/mailbox/:userId',
            name: 'mailbox',
            component: Mailbox,
            meta: {title: 'Messagerie'}
        },
        {
            path: '/last-orders',
            name: 'lastOrders',
            component: LastOrders,
            meta: {title: 'Dernières commandes'}
        }
    ]
})

async function isUserConnected(route) {
    try {
        const response = await fetch('/data/users/users.json');
        if (!response.ok) {
            throw new Error();
        }
        const users = await response.json();
        const token = localStorage.getItem('token');

        const connectedUser = users.find(user => {
            return user.token === token;
        })

        return connectedUser !== undefined;
    } catch (error) {
        return false;
    }
}
router.beforeEach(async (to, from,) => {
    document.title = to.meta?.title ?? 'KidstoB';
    const canAccess = await isUserConnected(to);

    const excludedRoutes = [
        'sign-in',
        'sign-up',
        'contact'
    ]

    useUserStore().isConnectedUser = canAccess;

    if (!canAccess && !excludedRoutes.includes(to.name)) {
        return {name: 'sign-in'};
    } else if(canAccess && to.name === 'sign-in') {
        return {name : 'home'};
    }
})
export default router
